#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Engine
{
    private:
        vector<int>           weight;
        vector<int>           value;
        vector< vector<int> > matrixVector;
        int                   maxWeight;
    
        void initializeMatrix()
        {
            
            int len = (int)weight.size();
            for(int i = 0 ; i < len ; i++)
            {
                vector<int> innerVector;
                for(int j = 0 ; j <= maxWeight ; j++)
                {
                    innerVector.push_back(0);
                }
                matrixVector.push_back(innerVector);
            }
        }
    
        void displayMatrix()
        {
            int len = (int)weight.size();
            for(int i = 0 ; i < len ; i++)
            {
                for(int j = 0 ; j <= maxWeight ; j++)
                {
                    cout<<matrixVector[i][j]<<" ";
                }
                cout<<endl;
            }
        }
    
    public:
        Engine(vector<int> w , vector<int> v , int mW)
        {
            weight    = w;
            value     = v;
            maxWeight = mW;
            initializeMatrix();
        }
    
        void populateMatrixWithMaxValues()
        {
            int len = (int)weight.size();
            for(int i = 0 ; i < len ; i++)
            {
                for(int j = 1 ; j <= maxWeight ; j++)
                {
                    if(!i)
                    {
                        matrixVector[i][j] = value[i];
                    }
                    else
                    {
                        if(j < weight[i])
                        {
                            matrixVector[i][j] = matrixVector[i-1][j];
                        }
                        else
                        {
                            matrixVector[i][j] = max(matrixVector[i-1][j] , value[i] + matrixVector[i-1][j-weight[i]]);
                        }
                    }
                }
            }
        }
    
        int getMaxValue()
        {
            return matrixVector[(int)weight.size()-1][maxWeight];
        }
    
        void printAllWeights()
        {
            int i = (int)weight.size();
            int j = maxWeight;
            while(--i > 0)
            {
                if(matrixVector[i][j] != matrixVector[i-1][j])
                {
                    cout<<weight[i]<<" ";
                }
            }
            cout<<endl;
        }
};

int main(int argc, const char * argv[])
{
//    vector<int> weightVector = {1 , 3 , 4 , 5};
//    vector<int> valueVector  = {1 , 4 , 5 , 7};
//    int         maxWeight    = 7;
    vector<int> weightVector = {2,2,4,5};
    vector<int> valueVector  = {3,7,2,9};
    int         maxWeight    = 10;
    Engine      e            = Engine(weightVector , valueVector , maxWeight);
    e.populateMatrixWithMaxValues();
    cout<<e.getMaxValue()<<endl;
    e.printAllWeights();
    return 0;
}
